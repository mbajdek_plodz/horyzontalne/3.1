package owncache;
import java.util.*;

/**
 * Created by Pietras on 2017-05-14.
 */
public class CacheMapExample<K, T> {

    private long timeToLive;
    private HashMap<K, T> cacheMap;

    protected static class CacheMapObject {
        public long lastAccessed = System.currentTimeMillis();
        public String value;

        protected CacheMapObject(String value) {
            this.value = value;
        }
    }

    public CacheMapExample(long objectLifetime, final long checkInterval, int maxElements) {
        this.timeToLive = objectLifetime * 1000;

        cacheMap = new HashMap<K, T>(maxElements);

        if (objectLifetime > 0 && checkInterval > 0) {

            Thread t = new Thread(new Runnable() {
                public void run() {
                    while (true) {
                        try {
                            Thread.sleep(checkInterval * 1000);
                            cleanup();
                        } catch (InterruptedException ex) {
                        }

                    }
                }
            });

            t.setDaemon(true);
            t.start();
        }
    }

    // PUT method
    public void put(K key, T value) {
        synchronized (cacheMap) {
            cacheMap.put(key, value);
        }
    }

    // GET method
    @SuppressWarnings("unchecked")
    public T get(K key) {
        synchronized (cacheMap) {
            CacheMapObject c = (CacheMapObject) cacheMap.get(key);

            if (c == null)
                return null;
            else {
                c.lastAccessed = System.currentTimeMillis();
                return (T) c.value;
            }
        }
    }

    // REMOVE method
    public void remove(String key) {
        synchronized (cacheMap) {
            cacheMap.remove(key);
        }
    }

    // Get Cache Objects Size()
    public int size() {
        synchronized (cacheMap) {
            return cacheMap.size();
        }
    }

    public boolean containsKey(K key){
        synchronized (cacheMap){
            return cacheMap.containsKey(key);
        }
    }

    // CLEANUP method
    public void cleanup() {

        long now = System.currentTimeMillis();
        ArrayList<String> deleteKey = null;

        synchronized (cacheMap) {
            Iterator<?> itr = cacheMap.keySet().iterator();

            deleteKey = new ArrayList<String>((cacheMap.size() / 2) + 1);
            CacheMapObject c = null;

            while (itr.hasNext()) {
                String key = (String) itr.next();
                c = (CacheMapObject) cacheMap.get(key);
                if (c != null && (now > (timeToLive + c.lastAccessed))) {
                    deleteKey.add(key);
                }
            }
        }

        for (String key : deleteKey) {
            synchronized (cacheMap) {
                cacheMap.remove(key);
            }

            Thread.yield();
        }
    }
}