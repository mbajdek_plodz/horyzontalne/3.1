package owncache;

public class Main {

    private static final String TEST_KEY = "test-key";
    private static final String TEST_VALUE = "test-value";


    public static void main(String[] args) throws InterruptedException {
        CacheMapExample<String, CacheMapExample.CacheMapObject> cacheMapExample = new CacheMapExample<>(1, 10, 50);
        CacheMapExample.CacheMapObject cacheMapObject = new CacheMapExample.CacheMapObject(TEST_VALUE);
        cacheMapExample.put(TEST_KEY, cacheMapObject);
        for(int i=0;i<100;i++){
            System.out.println("element exists? "+TEST_KEY+" exists...");
            System.out.println("exists? : "+cacheMapExample.containsKey(TEST_KEY));
            Thread.sleep(50000);
        }
    }
}
